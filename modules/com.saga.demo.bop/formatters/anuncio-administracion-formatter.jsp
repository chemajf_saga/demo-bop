<%@page import="java.util.Map"%>
<%@page contentType="text/html; charset=UTF-8" buffer="none" session="false" trimDirectiveWhitespaces="true"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib tagdir="/WEB-INF/tags/bop" prefix="bop"%>
<%@taglib uri="http://www.opencms.org/taglib/cms" prefix="cms"%>
<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.sagasuite.core.messages">
    <cms:formatter var="content" val="value" rdfa="rdfa">
        <c:choose>
            <c:when test="${cms.element.inMemoryOnly}">
                <h1 class="title">Edite este recurso!!</h1>
            </c:when>
            <c:otherwise>
                <%-- Enlace al detalle --%>
                <c:set var="enlaceDetalle">
                    <cms:link>${content.filename}</cms:link>
                </c:set>
                <%-- Vemos si tenemos que realizar alguna ACCION --%>
                <c:if test="${not empty param.acion}">
                    <%-- Tenemos que cambiar el estado del anuncio --%>
                    <c:set var="resultadoEstado">
                        <bop:cambiar-estado-administracion docRevisada="${param.docRevisada}" anuncioRevisado="${param.anuncioRevisado}"
                            precioCorrecto="${param.precioCorrecto}" anuncioPagado="${param.anuncioPagado}" cambio="${param.cambio}"
                            fileName="${content.filename}" motivoRechazo="${param.motivoRechazo}" />
                    </c:set>
                    <c:if test="${not empty resultadoEstado}">
                        <%
                            response.sendRedirect(pageContext.getAttribute("enlaceDetalle").toString());
                        %>
                    </c:if>
                    <c:if test="${empty resultadoEstado}">
                    </c:if>
                </c:if>
                <%-- ******************************************* --%>
                <%-- Cargamos la variable con el id especifico del recurso para su uso posterior--%>
                <c:set var="idResource" value="${content.id}" scope="request"></c:set>
                <%-- Tratamos el detalle de entidad (categoria) --%>
                <c:set var="entidadFinal">
                    <bop:entidad nivel="2" entidades="${value.DetalleEntidad}" />
                </c:set>
                <%-- Plazo de publicacion --%>
                <c:set var="plazoPublicacion" value="" />
                <c:if test="${value.PlazoDePublicacion.value.PlazoOrdinario.isSet}">
                    <c:set var="plazoPublicacion" value="Ordinario" />
                </c:if>
                <c:if test="${value.PlazoDePublicacion.value.PlazoUrgente.isSet}">
                    <c:set var="plazoPublicacion" value="Urgente" />
                </c:if>
                <c:if test="${value.PlazoDePublicacion.value.PlazoEspecifico.isSet}">
                    <c:set var="plazoPublicacion">
                        <bop:fecha fecha="${value.PlazoDePublicacion.value.PlazoEspecifico.value.PlazoEspecificoValue}" patron="dd/MM/yyyy" />
                    </c:set>
                </c:if>
                <c:set var="verPrecio" value="" />
                <c:if test="${value.Precio.isSet}">
                    <c:set var="verPrecio" value="${value.Precio}" />
                </c:if>
                <c:if test="${not value.Precio.isSet}">
                    <c:set var="verPrecio" value="0" />
                </c:if>
                <%--  --%>
                <div class="detalle-anuncio">
                    <%-- Botonera de acciones.. Según el estado del anuncio se mostraran los botonoes de acción --%>
                    <%-- Estados para el TRAMITADOR: nuevo, pendiente_pago, en_maquetacion, rechazado o acetado --%>
                    <c:set var="NUEVO" value="nuevo" />
                    <c:set var="PENDIENTE_PAGO" value="pendiente_pago" />
                    <c:set var="EN_MAQUETACION" value="en_maquetacion" />
                    <c:set var="RECHAZADO" value="rechazado" />
                    <c:set var="ACEPTADO" value="acetado" />
                    <%--  --%>
                    <div id="modales">
                        <%-- MODAL LOADING --%>
                        <div class="modal fade loading" id="modalCargando" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"
                            style="z-index: 999999999999999;">
                            <div class="modal-dialog" role="document">
                                <div id="LottieContainer" class="modal-content" style="min-height: 600px">
                                
                                
                                    <div class="loading-animation">
                                        <img src="/system/modules/com.saga.demo.bop/resources/animations/Rolling-1s-200px.svg">
                                    </div>
                                    <div class="txt-cargando">
                                        <p>CARGANDO...</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <c:choose>
                            <c:when test="${value.EstadoParaElTramitador eq NUEVO}">
                                <div class="advice-state">                              
                                    <button type="button" class="btn btn-info" style="cursor: default;">
                                        <bop:estado-to-display estado="${value.EstadoParaElTramitador}" />
                                    </button>
                                </div>
                                <div class="action-buttons">
                                    <!-- <button type="button" class="btn btn-primary aceptar" data-toggle="modal" data-target="#modalCargando">cargando</button> -->
                                    <button type="button" class="btn btn-primary aceptar" data-toggle="modal" data-target="#modalAceptarNuevo">Aceptar</button>
                                    <button type="button" class="btn btn-primary rechazar" data-toggle="modal" data-target="#modalRechazarNuevo">Rechazar</button>
                                </div>
                                <%--  --%>
                                <div class="modal fade cambio-estado" id="modalAceptarNuevo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                    aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Cambio de Estado del Anuncio</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form action="${enlaceDetalle}" method="get">
                                                    <div class="form-group">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="checkbox" id="docRevisada" name="docRevisada" /> <label
                                                                class="form-check-label" for="docRevisada">Documentación revisada</label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="checkbox" id="anuncioRevisado"
                                                                name="anuncioRevisado" /> <label class="form-check-label" for="anuncioRevisado">Anuncio
                                                                Revisado</label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="checkbox" id="precioCorrecto" name="precioCorrecto" />
                                                            <label class="form-check-label" for="precioCorrecto">¿Precio
                                                                correcto?:&nbsp;${verPrecio}</label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="checkbox" id="anuncioPagado" name="anuncioPagado" />
                                                            <label class="form-check-label" for="anuncioPagado">¿Anuncio abonado?</label>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="cambio">Seleccione el siguiente estado</label> <select id="cambio" name="cambio"
                                                            class="form-control">
                                                            <option value="en_maquetacion">Maquetar</option>
                                                            <option value="pendiente_pago">Pendiente de Pago</option>
                                                        </select>
                                                    </div>
                                                    <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#modalCargando">Cambiar</button>
                                                    <input type="hidden" id="acion" name="acion" value="si" />
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <%--  --%>
                                <div class="modal fade cambio-estado" id="modalRechazarNuevo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                    aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Cambio de Estado del Anumcio</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form action="${enlaceDetalle}" method="post">
                                                    <div class="form-group">
                                                        <label for="motivoRechazo">Motivo del rechazo</label>
                                                        <textarea class="form-control" id="motivoRechazo" name="motivoRechazo" rows="3"></textarea>
                                                    </div>
                                                    <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#modalCargando">Rechazar</button>
                                                    <input type="hidden" id="acion" name="acion" value="si" /> <input type="hidden" id="cambio"
                                                        name="cambio" value="rechazado" />
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </c:when>
                            <c:when test="${value.EstadoParaElTramitador eq PENDIENTE_PAGO}">
                                <div class="advice-state">
                                    <button type="button" class="btn btn-warning" style="cursor: default;">
                                        <bop:estado-to-display estado="${value.EstadoParaElTramitador}" />
                                    </button>
                                </div>
                                <div class="action-buttons">
                                    <button type="button" class="btn btn-primary maquetar" data-toggle="modal" data-target="#modalMaquetarPP">Maquetar</button>
                                </div>
                                <div class="modal fade cambio-estado" id="modalMaquetarPP" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                    aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form action="${enlaceDetalle}" method="get">
                                                    <div class="form-group">
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="checkbox" id="anuncioPagado" name="anuncioPagado" />
                                                            <label class="form-check-label" for="anuncioPagado">¿Anuncio abonado?</label>
                                                        </div>
                                                    </div>
                                                    <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#modalCargando">Aceptar</button>
                                                    <input type="hidden" id="acion" name="acion" value="si" /> <input type="hidden" id="cambio"
                                                        name="cambio" value="en_maquetacion" />
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                                <button type="button" class="btn btn-primary">Save changes</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </c:when>
                            <c:when test="${value.EstadoParaElTramitador eq EN_MAQUETACION}">
                                <div class="advice-state">
                                    <button type="button" class="btn btn-dark" style="cursor: default;">
                                        <bop:estado-to-display estado="${value.EstadoParaElTramitador}" />
                                    </button>
                                </div>
                                <div class="action-buttons">
                                    <button type="button" class="btn btn-primary aceptar" data-toggle="modal" data-target="#modalAceptarMaquetado">Aceptar</button>
                                </div>
                                <div class="modal fade cambio-estado" id="modalAceptarMaquetado" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                    aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Aceptar anuncio</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form action="${enlaceDetalle}" method="get">
                                                    <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#modalCargando">Firmar</button>
                                                    <input type="hidden" id="acion" name="acion" value="si" /> <input type="hidden" id="cambio"
                                                        name="cambio" value="acetado" />
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </c:when>
                            <c:when test="${value.EstadoParaElTramitador eq RECHAZADO}">
                                <div class="advice-state">
                                    <button type="button" class="btn btn-danger" style="cursor: default;">
                                        <bop:estado-to-display estado="${value.EstadoParaElTramitador}" />
                                    </button>
                                </div>
                                <div class="subjetct-reject">
                                    <div>Motivo del rechazo:</div>
                                    <div class="alert alert-danger" role="alert">${value.MotivoRechazo}</div>
                                </div>
                            </c:when>
                            <c:when test="${value.EstadoParaElTramitador eq ACEPTADO}">
                                <div class="advice-state">
                                    <button type="button" class="btn btn-success" style="cursor: default;">
                                        <bop:estado-to-display estado="${value.EstadoParaElTramitador}" />
                                    </button>
                                </div>
                            </c:when>
                        </c:choose>
                        <%--  --%>
                    </div>
                    <div class="identificacion-anuncio">
                        <div class="titulo-desc-anuncio">
                            <h2>${value.Title}</h2>
                            <h3>${value.Teaser}</h3>
                        </div>
                        <div class="info-entidad-anuncio">
                            <div class="rama-entidad-anuncio">
                                <h4>
                                    <span class="fa fa-folder" aria-hidden="true">&nbsp;</span>${entidadFinal}
                                </h4>
                            </div>
                            <c:if test="${value.Tramitador.value.Nombre.isSet}">
                                <div class="tramitador-anuncio">
                                    <h4>
                                        <span class="fa fa-user" aria-hidden="true">&nbsp;</span>Tramitador: ${value.Tramitador.value.Nombre}
                                    </h4>
                                </div>
                            </c:if>
                            <c:if test="${value.Tramitador.value.Mail.isSet}">
                                <div class="email-tramitador-anuncio">
                                    <h4>
                                        <span class="fa fa-envelope" aria-hidden="true">&nbsp;</span>Email: ${value.Tramitador.value.Mail}
                                    </h4>
                                </div>
                            </c:if>
                            <c:if test="${value.Tramitador.value.Telefono.isSet}">
                                <div class="telefono-tramitador-anuncio">
                                    <h4>
                                        <span class="fa fa-phone" aria-hidden="true">&nbsp;</span>Teléfono: ${value.Tramitador.value.Telefono}
                                    </h4>
                                </div>
                            </c:if>
                        </div>
                        <div class="pasivo-anuncio">
                            <c:if test="${value.SujetoPasivo.isSet}">
                                <div class="sujeto-pasivo-anuncio">
                                    <h4>
                                        <span class="fa fa-user" aria-hidden="true">&nbsp;</span> <span class="sujeto">Sujeto pasivo:</span>
                                        ${value.SujetoPasivo}
                                    </h4>
                                </div>
                            </c:if>
                            <div class="precio-anuncio">
                                <h4>
                                    <span class="fa fa-money" aria-hidden="true">&nbsp;</span> <span class="sujeto">Precio:&nbsp;</span> <span
                                        class="precio">${verPrecio}€</span>
                                </h4>
                            </div>
                        </div>
                        <div class="plazo-anuncio">
                            <div class="plazo-publicacion">
                                <p>
                                    <span class="fa fa-calendar" aria-hidden="true">&nbsp;</span>Plazo publicación: ${plazoPublicacion}
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="contenido-anuncio">
                        <div class="texto-anuncio">
                            <div class="texto-anuncio-tit">
                                <h4>Texto del anuncio</h4>
                            </div>
                            <div class="contenido-texto-anuncio">${value.TextoDelAnuncio}</div>
                        </div>
                        <c:if test="${value.TextoFinalDelAnuncio.isSet}">
                            <div class="texto-anuncio">
                                <div class="texto-anuncio-tit">
                                    <h4>Texto final del anuncio</h4>
                                </div>
                                <div class="contenido-texto-anuncio">${value.TextoFinalDelAnuncio}</div>
                            </div>
                        </c:if>
                        <div class="ficheros-anuncio">
                            <c:set var="ficheroTitle">
                                <cms:property name="Title_${cms.locale}" file="${value.Fichero}" />
                            </c:set>
                            <c:if test="${empty ficheroTitle}">
                                <c:set var="ficheroTitle">
                                    <cms:property name="Title" file="${value.Fichero}" />
                                </c:set>
                            </c:if>
                            <div class="ficheros-anuncio-tit">
                                <h4>Fichero</h4>
                            </div>
                            <div class="ficheros-anuncio-listado">
                                <h5>
                                    <a href="<cms:link>${value.Fichero}</cms:link>">${ficheroTitle}</a>
                                </h5>
                            </div>
                        </div>
                        <c:if test="${value.Anexos.isSet}">
                            <div class="ficheros-anuncio">
                                <div class="ficheros-anuncio-tit">
                                    <h4>Anexos</h4>
                                </div>
                                <div class="ficheros-anuncio-listado">
                                    <ul>
                                        <c:forEach items="${content.valueList.Anexos}" var="anexo">
                                            <c:set var="anexoTitle">
                                                <cms:property name="Title_${cms.locale}" file="${anexo}" />
                                            </c:set>
                                            <c:if test="${empty anexoTitle}">
                                                <c:set var="anexoTitle">
                                                    <cms:property name="Title" file="${anexo}" />
                                                </c:set>
                                            </c:if>
                                            <li><a href="<cms:link>${anexo}</cms:link>">${anexoTitle}</a></li>
                                        </c:forEach>
                                    </ul>
                                </div>
                            </div>
                        </c:if>
                    </div>
                </div>
            </c:otherwise>
        </c:choose>
    </cms:formatter>
</cms:bundle>