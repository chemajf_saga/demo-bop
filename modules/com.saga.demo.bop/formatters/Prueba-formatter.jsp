<%@page buffer="none" session="false" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.sagasuite.core.messages">
<cms:formatter var="content" val="value" rdfa="rdfa">
<c:choose>
	<c:when test="${cms.element.inMemoryOnly}">
				<h1 class="title">
					Edite este recurso!!
				</h1>
	</c:when>
<c:otherwise>
<%-- Cargamos la variable con el id especifico del recurso para su uso posterior--%>
<c:set var="idresource" value="${content.id}" scope="request"></c:set>

<c:if test="${cms.element.setting.marginbottom.value != '0'}">
    <c:set var="marginClass">margin-bottom-${cms.element.setting.marginbottom.value}</c:set>
</c:if>
<c:if test="${not empty cms.element.settings.classmainbox }">
    <c:set var="classmainbox">${cms.element.settings.classmainbox}</c:set>
</c:if>

<%-- Definimos el tipo de etiqueta html para nuestro encabezado --%>

<c:set var="titletag">h1</c:set>
<c:set var="subtitletag">h2</c:set>

<c:if test="${not empty cms.element.settings.titletag }">
	<c:set var="titletag">${cms.element.settings.titletag }</c:set>
	<c:if test="${titletag == 'h1'}">
		<c:set var="subtitletag">h2</c:set>
	</c:if>
	<c:if test="${titletag == 'h2'}">
		<c:set var="subtitletag">h3</c:set>
	</c:if>
	<c:if test="${titletag == 'h3'}">
		<c:set var="subtitletag">h4</c:set>
	</c:if>
	<c:if test="${titletag == 'h4'}">
		<c:set var="subtitletag">h5</c:set>
	</c:if>	
</c:if>

<article class="articulo element parent <c:out value=' ${marginClass} ${classmainbox} ' />">

<div class="wrapper <c:out value='${value.CssClass} ' />">

<c:if test="${not cms.element.settings.hidetitle}">
	<!-- Cabecera del articulo -->
	<header class="headline">
		<c:if test="${value.SubTitle.isSet }">
		<hgroup>
		</c:if>
			<${titletag} class="title" ${rdfa.Title}>${value.Title}</${titletag}>
			<c:if test="${value.SubTitle.isSet }">
					<${subtitletag} class="subtitle" ${rdfa.SubTitle}>${value.SubTitle}</${subtitletag}>
				</hgroup>
			</c:if>
	</header>
</c:if>

<c:if test="${value.Content.exists}">

<c:forEach var="contentList" items="${content.valueList.Content}" varStatus="status">
	<c:set var="contentNameField">Content[${status.count }]</c:set>
	<c:forEach var="elem" items="${content.subValueList[contentNameField]}" varStatus="status2">
		<c:choose>
			<%-- TEXTO SIMPLE --%>
			<c:when test="${elem.name == 'TextSimple'}">
				<c:set var="contentblock" value="${elem}" scope="request"></c:set>				
				<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.core/elements/e-simpletext.jsp:ea1d0a2b-df65-11e4-bcf9-01e4df46f753)">
					<cms:param name="contPrincipal">${status.count }</cms:param>
					<cms:param name="contSecundario">${status2.count}</cms:param>
				</cms:include>
			</c:when>
			<%-- BLOQUE DE TEXTO --%>
			<c:when test="${elem.name == 'TextBlock'}">
				<c:set var="contentblock" value="${elem}" scope="request"></c:set>
				<%--Si se ha configurado una plantilla del bloque de contenido llamamos a dicha plantilla, si no llamamos a la de por defecto --%>
				<c:if test="${contentblock.value.Template.exists}">
					<cms:include file="${contentblock.value.Template}">
						<cms:param name="contentBlockCount">${status.count}</cms:param>
						<cms:param name="sectionCount">${param.sectionCount }</cms:param>
						<cms:param name="contPrincipal">${status.count }</cms:param>
						<cms:param name="contSecundario">${status2.count}</cms:param>
						<cms:param name="tabs">false</cms:param>
					</cms:include>				
				</c:if>
				<c:if test="${!contentblock.value.Template.exists}">				
					<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.core/elements/e-default-contentblock.jsp:e9a36d37-df65-11e4-bcf9-01e4df46f753)">
						<cms:param name="contentBlockCount">1</cms:param>
						<cms:param name="sectionCount">1</cms:param>
						<cms:param name="contPrincipal">${status.count }</cms:param>
						<cms:param name="contSecundario">${status2.count}</cms:param>
						<cms:param name="tabs">false</cms:param>
					</cms:include>
				</c:if>
			</c:when>
			<%-- MULTIPLES BLOQUES 
			<c:when test="${elem.name == 'ListBlock'}">
			
				<!-- Bloques de contenido de la seccion -->
				<c:forEach var="contentblock" items="${elem.valueList.TextBlock}" varStatus="statusSection">
					<c:set var="contentblock" value="${contentblock}" scope="request"></c:set>
					<%--Si se ha configurado una plantilla del bloque de contenido llamamos a dicha plantilla, si no llamamos a la de por defecto -- %>
					<c:if test="${contentblock.value.Template.exists}">
						<cms:include file="${contentblock.value.Template}">
							<cms:param name="contentBlockCount">${statusSection.count}</cms:param>
							<cms:param name="sectionCount">${param.sectionCount }</cms:param>
							<cms:param name="tabs">false</cms:param>
						</cms:include>
					</c:if>
					<c:if test="${!contentblock.value.Template.exists}">
						<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.core/elements/e-default-contentblock.jsp:e9a36d37-df65-11e4-bcf9-01e4df46f753)">
							<cms:param name="contentBlockCount">${statusSection.count}</cms:param>
							<cms:param name="sectionCount">${param.sectionCount }</cms:param>
							<cms:param name="tabs">false</cms:param>
						</cms:include>
					</c:if>
				</c:forEach>
			</c:when>
			--%>
			<%-- SECCIONES --%>
			<c:when test="${elem.name == 'ListSection'}">
			
				<c:set var="viewMode" value="${elem.value.ViewMode}" scope="request"/>
				<c:set var="navPosition" value="${elem.value.NavPosition}" scope="request"/>
				<c:set var="menuId" value="${elem.value.MenuId}" scope="request"/>

				<c:if test="${elem.value.MenuTitle.exists and elem.value.MenuTitle.isSet}">
					<c:set var="menuTitle" value="${elem.value.MenuTitle}" scope="request"/>
				</c:if>				
				<c:set var="listSection" value="${elem}" scope="request"/>
				<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.core/elements/e-listsections.jsp:e9eafd9f-df65-11e4-bcf9-01e4df46f753)">
					<cms:param name="date">${value.Date }</cms:param>
					<cms:param name="showtime">false</cms:param>
					<cms:param name="contPrincipal">${status.count }</cms:param>
					<cms:param name="contSecundario">${status2.count}</cms:param>
				</cms:include>
			</c:when>
		</c:choose>
	</c:forEach>
</c:forEach>

</c:if> <%-- Fin cierre comprobacion de seccion --%>

	<!-- Pie del articulo -->
	<c:if test="${content.value.ShowFooter == 'true' }">
		<div class="posted">
			<span class="fa fa-calendar" aria-hidden="true"></span>&nbsp;&nbsp;
			<cms:include file="%(link.strong:/system/modules/com.saga.sagasuite.core/elements/e-time.jsp:ea2c737f-df65-11e4-bcf9-01e4df46f753)">
				<cms:param name="date">${value.Date }</cms:param>
				<cms:param name="showtime">false</cms:param>
			</cms:include>
		</div>
		<c:if test="${value.Author.exists }">
			<div class="autor"><span class="fa fa-user" aria-hidden="true"></span>&nbsp;&nbsp;${value.Author.value.Author }</div>
		</c:if>
		<c:if test="${value.Source.isSet }">
			<div class="fuente"><span class="fa fa-book" aria-hidden="true"></span>&nbsp;&nbsp;${value.Source}</div>
		</c:if>
	</c:if>
	
</div> <!-- Fin de wrapper -->
</article>

</c:otherwise>
</c:choose>
</cms:formatter>
</cms:bundle>