<%@page buffer="none" session="false" trimDirectiveWhitespaces="true"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@taglib prefix="bop" tagdir="/WEB-INF/tags/bop"%>
<fmt:setLocale value="${cms.locale}" />
<cms:bundle basename="com.saga.sagasuite.core.messages">
    <cms:formatter var="content" val="value" rdfa="rdfa">
        <c:choose>
            <c:when test="${cms.element.inMemoryOnly}">
                <h1 class="title">Edite este recurso!!</h1>
            </c:when>
            <c:otherwise>
                <%-- Enlace al detalle --%>
                <c:set var="enlaceDetalle">
                    <cms:link>${content.filename}</cms:link>
                </c:set>
                <%-- Cargamos la variable con el id especifico del recurso para su uso posterior--%>
                <c:set var="idresource" value="${content.id}" scope="request"></c:set>
                <%-- Vemos si tenemos que realizar alguna ACCION --%>
                <c:if test="${not empty param.acion}">
                    <%-- Tenemos que cambiar el estado del anuncio --%>
                    <c:set var="resultadoEstado">
                        <bop:cambiar-estado-bop fileName="${content.filename}" cambio="${param.cambio}" />
                    </c:set>
                    <c:if test="${not empty resultadoEstado}">
                        <%
                            response.sendRedirect(pageContext.getAttribute("enlaceDetalle").toString());
                        %>
                    </c:if>
                    <c:if test="${empty resultadoEstado}">
                    </c:if>
                </c:if>
                <%-- ******************************************* --%>
                <%-- Formateamos fecha --%>
                <c:set var="fechaPublicacionFormated">
                    <bop:fecha fecha="${content.value.FechaPublicacion}" patron="EEEE d 'de' MMMM 'de' yyyy" may="sep" />
                </c:set>
                <%--  --%>
                <c:set var="EN_CURSO" value="en_curso" />
                <c:set var="P_PUBLICACION" value="p_publicacion" />
                <c:set var="PUBLICADO" value="publicado" />
                <%--  --%>
                <div class="bop-total">
                    <div class="" id="modales" style="margin: 0 0 15px 0;">
                        <%-- MODAL LOADING --%>
                        <div class="modal fade" id="modalCargando" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"
                            style="z-index: 999999999999999;">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content" style="min-height: 600px">CARGANDO...</div>
                            </div>
                        </div>
                        <c:choose>
                            <c:when test="${value.Estado eq EN_CURSO}">
                                <button type="button" class="btn btn-info" style="cursor: default;">
                                    <bop:estado-to-display estado="${value.Estado}" />
                                </button>
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalFirmar">Firmar para la
                                    publicación</button>
                                <%--  --%>
                                <div class="modal fade" id="modalFirmar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                                    aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Cambio de Estado: Firmar para la publicación</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <form action="${enlaceDetalle}" method="get">
                                                    <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#modalCargando">Firmar</button>
                                                    <input type="hidden" id="acion" name="acion" value="si" /> <input type="hidden" id="cambio"
                                                        name="cambio" value="p_publicacion" />
                                                </form>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <%--  --%>
                            </c:when>
                            <c:when test="${value.Estado eq P_PUBLICACION}">
                                <button type="button" class="btn btn-warning" style="cursor: default;">
                                    <bop:estado-to-display estado="${value.Estado}" />
                                </button>
                            </c:when>
                            <c:when test="${value.Estado eq PUBLICADO}">
                                <button type="button" class="btn btn-success" style="cursor: default;">
                                    <bop:estado-to-display estado="${value.Estado}" />
                                </button>
                            </c:when>
                        </c:choose>
                        <a class="btn btn-default btn-sm btn-pdf hidden-xs hidden-xxs pull-right hastooltip" title="PDF"
                            href="<cms:pdf format='%(link.weak:/system/modules/com.saga.demo.bop/functions/f-bop-pdf.jsp)' content='${content.filename}' locale='${locale}'/>"
                            target="pdf"> PDF </a>
                    </div>
                    <div class="cabecera-bops">
                        <div class="logo">
                            <img title="cabecera-bop"
                                src="<cms:link>/sites/gestion-bop-dipu-sevilla/.content/.template-elements/assets/cabecera-bop.png</cms:link>"
                                alt="cabecera-bop" />
                        </div>
                        <div class="texto">
                            <div class="fecha">
                                <p>${fechaPublicacionFormated}</p>
                            </div>
                            <div class="numero">
                                <p>Número ${content.value.CodigoBop}</p>
                            </div>
                        </div>
                    </div>
                    <div class="detalle-bops">
                        <%--SUMARIO--%>
                        <div class="sumario">
                            <c:set var="entidadN2" value="" />
                            <c:set var="entidadN3" value="" />
                            <c:set var="entidadN4" value="" />
                            <div class="titulo">
                                <h1>Sumario</h1>
                            </div>
                            <div class="listado-sumario">
                                <c:forEach items="${content.valueList.Anuncios}" var="anuncio">
                                    <%--recuperamos el titulo--%>
                                    <c:set var="anuncioTitle">
                                        <cms:property name="Title_${cms.locale}" file="${anuncio}" />
                                    </c:set>
                                    <c:if test="${empty anuncioTitle}">
                                        <c:set var="anuncioTitle">
                                            <cms:property name="Title" file="${anuncio}" />
                                        </c:set>
                                    </c:if>
                                    <cms:contentload collector="singleFile" param="${anuncio}">
                                        <cms:contentaccess var="resourceAnuncio" scope="request" />
                                    </cms:contentload>
                                    <c:set var="entidadN2Aux">
                                        <bop:entidad nivel="2" entidades="${resourceAnuncio.value.DetalleEntidad}" concreto="sep" />
                                    </c:set>
                                    <c:set var="entidadN3Aux">
                                        <bop:entidad nivel="3" entidades="${resourceAnuncio.value.DetalleEntidad}" concreto="sep" />
                                    </c:set>
                                    <c:set var="entidadN4Aux">
                                        <bop:entidad nivel="4" entidades="${resourceAnuncio.value.DetalleEntidad}" concreto="sep" />
                                    </c:set>
                                    <c:if test="${not (entidadN2 eq entidadN2Aux)}">
                                        <c:set var="entidadN2" value="${entidadN2Aux}" />
                                        <h2 class="subseccion-sumario">${entidadN2}</h2>
                                    </c:if>
                                    <c:if test="${entidadN2 eq entidadN2Aux}">
                                        <c:if test="${not (entidadN3 eq entidadN3Aux)}">
                                            <c:set var="entidadN3" value="${entidadN3Aux}" />
                                            <h3 class="entidad-sumario">${entidadN3}</h3>
                                        </c:if>
                                        <c:if test="${entidadN3 eq entidadN3Aux}">
                                            <c:if test="${not (entidadN4 eq entidadN4Aux)}">
                                                <c:set var="entidadN4" value="${entidadN4Aux}" />
                                                <h4 class="detalle-sumario">${entidadN4}:</h4>
                                            </c:if>
                                            <c:if test="${entidadN4 eq entidadN4Aux}">
                                            </c:if>
                                        </c:if>
                                    </c:if>
                                    <h5 class="anuncio-sumario">
                                        <a href="<cms:link>${anuncio}</cms:link>">${anuncioTitle}</a>
                                    </h5>
                                </c:forEach>
                            </div>
                        </div>
                        <%--TEXTOS--%>
                        <div class="anuncios">
                            <div class="imagencabecera">
                                <img title="cabecera-anuncios-bop"
                                    src="<cms:link>/sites/gestion-bop-dipu-sevilla/.content/.template-elements/assets/cabecera-anuncios-bop.png</cms:link>"
                                    alt="cabecera-anuncios-bop" />
                            </div>
                            <div class="texto">
                                <div class="fecha">
                                    <p>${fechaPublicacionFormated}</p>
                                </div>
                                <div class="numero">
                                    <p>Número ${content.value.CodigoBop}</p>
                                </div>
                            </div>
                            <c:set var="entidadN2" value="" />
                            <c:set var="entidadN3" value="" />
                            <c:set var="entidadN4" value="" />
                            <c:forEach items="${content.valueList.Anuncios}" var="anuncio">
                                <cms:contentload collector="singleFile" param="${anuncio}">
                                    <cms:contentaccess var="resourceAnuncio" scope="request" />
                                </cms:contentload>
                                <c:set var="entidadN2Aux">
                                    <bop:entidad nivel="2" entidades="${resourceAnuncio.value.DetalleEntidad}" concreto="sep" />
                                </c:set>
                                <c:set var="entidadN3Aux">
                                    <bop:entidad nivel="3" entidades="${resourceAnuncio.value.DetalleEntidad}" concreto="sep" />
                                </c:set>
                                <c:set var="entidadN4Aux">
                                    <bop:entidad nivel="4" entidades="${resourceAnuncio.value.DetalleEntidad}" concreto="sep" />
                                </c:set>
                                <c:if test="${not (entidadN2 eq entidadN2Aux)}">
                                    <c:set var="entidadN2" value="${entidadN2Aux}" />
                                    <h2 class="subseccion">${entidadN2}</h2>
                                </c:if>
                                <c:if test="${entidadN2 eq entidadN2Aux}">
                                    <c:if test="${not (entidadN3 eq entidadN3Aux)}">
                                        <c:set var="entidadN3" value="${entidadN3Aux}" />
                                        <h3 class="entidad">${entidadN3}</h3>
                                    </c:if>
                                    <c:if test="${entidadN3 eq entidadN3Aux}">
                                        <c:if test="${not (entidadN4 eq entidadN4Aux)}">
                                            <c:set var="entidadN4" value="${entidadN4Aux}" />
                                            <h4 class="detalle-entidad">${entidadN4}</h4>
                                        </c:if>
                                        <c:if test="${entidadN4 eq entidadN4Aux}">
                                        </c:if>
                                    </c:if>
                                </c:if>
                                <div class="anuncio-contenido">${resourceAnuncio.value.TextoDelAnuncio}</div>
                            </c:forEach>
                        </div>
                        <div class="tasas-bop-wrapper">
                            <div class="tasas-bop">
                                <div class="titular">
                                    <h2>
                                        TASAS CORRESPONDIENTES AL<br />«BOLETÍN OFICIAL» DE LA PROVINCIA DE SEVILLA
                                    </h2>
                                </div>
                                <div class="precios">
                                    <div class="col-izda">
                                        <div class="lineas-precio">
                                            <div class="texto">
                                                <p>Inserción anuncio, línea ordinaria</p>
                                            </div>
                                            <div class="precio">
                                                <p>2,10</p>
                                            </div>
                                        </div>
                                        <div class="lineas-precio">
                                            <div class="texto">
                                                <p>Inserción anuncio, línea urgente</p>
                                            </div>
                                            <div class="precio">
                                                <p>3,25</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-dcha">
                                        <div class="lineas-precio">
                                            <div class="texto">
                                                <p>Importe mínimo de inserción</p>
                                            </div>
                                            <div class="precio">
                                                <p>18,41</p>
                                            </div>
                                        </div>
                                        <div class="lineas-precio">
                                            <div class="texto">
                                                <p>Venta de CD’s publicaciones anuales</p>
                                            </div>
                                            <div class="precio">
                                                <p>5,72</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="nota-final">
                                    <p class="primer-parrafo">Las solicitudes de inserción de anuncios, así como la correspondencia de tipo
                                        administrativo y económico, se dirigirán al «Boletín Oficial» de la provincia de Sevilla, avenida Menéndez y
                                        Pelayo, 32. 41071-Sevilla.</p>
                                    <p>Dirección del «Boletín Oficial» de la provincia de Sevilla: Ctra. Isla Menor, s/n. (Bellavista),
                                        41014-Sevilla. Teléfonos: 954 554 133 - 34 - 35 - 39. Faxes: 954 693 857 - 954 *0 649. Correo electrónico:
                                        bop@dipusevilla.es</p>
                                </div>
                            </div>
                            <div class="imprenta">
                                <p>Diputación Provincial - Imprenta</p>
                            </div>
                        </div>
                    </div>
                </div>
            </c:otherwise>
        </c:choose>
    </cms:formatter>
</cms:bundle>