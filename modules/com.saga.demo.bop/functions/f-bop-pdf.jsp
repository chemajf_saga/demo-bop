<%@page trimDirectiveWhitespaces="true" buffer="none" session="false" taglibs="c,cms,fmt,fn,bop"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@taglib prefix="bop" tagdir="/WEB-INF/tags/bop"%>


    <fmt:setLocale value="${cms.locale}" />
    <cms:bundle basename="com.saga.diputacion.huelva.frontend.messages">
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
        <html>
        <cms:formatter var="content">
            <head>
                <title>${content.value.CodigoBop}</title>

                <link rel="stylesheet" href="<cms:link>/sites/gestion-bop-dipu-sevilla/.content/.custom/custom-pdf.css</cms:link>" type="text/css" />
            </head>
            <body>
                <%-- Formateamos fecha --%>
            <c:set var="fechaPublicacionFormated">
                <bop:fecha fecha="${content.value.FechaPublicacion}" patron="EEEE d 'de' MMMM 'de' yyyy" may="sep" />
            </c:set>
            <div class="bop-total">
                <div class="cabecera-bops">
                    <div class="logo">
                        <img title="cabecera-bop" style="width: 100%"
                             src="<cms:link>/sites/gestion-bop-dipu-sevilla/.content/.template-elements/assets/cabecera-bop.png</cms:link>"
                             alt="cabecera-bop" />
                    </div>
                    <div class="texto">
                        <div class="fecha">
                            <p>${fechaPublicacionFormated}</p>
                        </div>
                        <div class="numero">
                            <p>Número ${content.value.CodigoBop}</p>
                        </div>
                    </div>
                </div>
                <div class="detalle-bops">
                        <%--SUMARIO--%>
                    <div class="sumario">
                        <c:set var="entidadN2" value="" />
                        <c:set var="entidadN3" value="" />
                        <c:set var="entidadN4" value="" />
                        <div class="titulo">
                            <h1>Sumario</h1>
                        </div>
                        <div class="listado-sumario">
                            <c:forEach items="${content.valueList.Anuncios}" var="anuncio">
                                <%--recuperamos el titulo--%>
                                <c:set var="anuncioTitle">
                                    <cms:property name="Title_${cms.locale}" file="${anuncio}" />
                                </c:set>
                                <c:if test="${empty anuncioTitle}">
                                    <c:set var="anuncioTitle">
                                        <cms:property name="Title" file="${anuncio}" />
                                    </c:set>
                                </c:if>
                                <cms:contentload collector="singleFile" param="${anuncio}">
                                    <cms:contentaccess var="resourceAnuncio" scope="request" />
                                </cms:contentload>
                                <c:set var="entidadN2Aux">
                                    <bop:entidad nivel="4" entidades="${resourceAnuncio.value.DetalleEntidad}" concreto="sep" />
                                </c:set>
                                <c:set var="entidadN3Aux">
                                    <bop:entidad nivel="5" entidades="${resourceAnuncio.value.DetalleEntidad}" concreto="sep" />
                                </c:set>
                                <c:set var="entidadN4Aux">
                                    <bop:entidad nivel="6" entidades="${resourceAnuncio.value.DetalleEntidad}" concreto="sep" />
                                </c:set>
                                <c:if test="${not (entidadN2 eq entidadN2Aux)}">
                                    <c:set var="entidadN2" value="${entidadN2Aux}" />
                                    <h2 class="subseccion-sumario">${entidadN2}</h2>
                                </c:if>
                                <c:if test="${entidadN2 eq entidadN2Aux}">
                                    <c:if test="${not (entidadN3 eq entidadN3Aux)}">
                                        <c:set var="entidadN3" value="${entidadN3Aux}" />
                                        <h3 class="entidad-sumario">${entidadN3}</h3>
                                    </c:if>
                                    <c:if test="${entidadN3 eq entidadN3Aux}">
                                        <c:if test="${not (entidadN4 eq entidadN4Aux)}">
                                            <c:set var="entidadN4" value="${entidadN4Aux}" />
                                            <h4 class="detalle-sumario">${entidadN4}:</h4>
                                        </c:if>
                                        <c:if test="${entidadN4 eq entidadN4Aux}">
                                        </c:if>
                                    </c:if>
                                </c:if>
                                <h5 class="anuncio-sumario">
                                    <a href="<cms:link>${anuncio}</cms:link>">${anuncioTitle}</a>
                                </h5>
                            </c:forEach>
                        </div>
                    </div>
                        <%--TEXTOS--%>
                    <div class="anuncios">
                        <div class="imagencabecera">
                            <img title="cabecera-anuncios-bop" style="width: 100%"
                                 src="<cms:link>/sites/gestion-bop-dipu-sevilla/.content/.template-elements/assets/cabecera-anuncios-bop.png</cms:link>"
                                 alt="cabecera-anuncios-bop" />
                        </div>
                        <div class="texto">
                            <div class="fecha">
                                <p>${fechaPublicacionFormated}</p>
                            </div>
                            <div class="numero">
                                <p>Número ${content.value.CodigoBop}</p>
                            </div>
                        </div>
                        <c:set var="entidadN2" value="" />
                        <c:set var="entidadN3" value="" />
                        <c:set var="entidadN4" value="" />
                        <c:forEach items="${content.valueList.Anuncios}" var="anuncio">
                            <cms:contentload collector="singleFile" param="${anuncio}">
                                <cms:contentaccess var="resourceAnuncio" scope="request" />
                            </cms:contentload>
                            <c:set var="entidadN2Aux">
                                <bop:entidad nivel="4" entidades="${resourceAnuncio.value.DetalleEntidad}" concreto="sep" />
                            </c:set>
                            <c:set var="entidadN3Aux">
                                <bop:entidad nivel="5" entidades="${resourceAnuncio.value.DetalleEntidad}" concreto="sep" />
                            </c:set>
                            <c:set var="entidadN4Aux">
                                <bop:entidad nivel="6" entidades="${resourceAnuncio.value.DetalleEntidad}" concreto="sep" />
                            </c:set>
                            <c:if test="${not (entidadN2 eq entidadN2Aux)}">
                                <c:set var="entidadN2" value="${entidadN2Aux}" />
                                <h2 class="subseccion">${entidadN2}</h2>
                                 <p class="separadorbops">
                                    <img class="separador" title="separador-bop" src="<cms:link>/sites/gestion-bop-dipu-sevilla/.content/.template-elements/assets/separador-bop.jpg</cms:link>"
                                 alt="separador-bop" />
                                 </p>
                            </c:if>
                            <c:if test="${entidadN2 eq entidadN2Aux}">
                                <c:if test="${not (entidadN3 eq entidadN3Aux)}">
                                    <c:set var="entidadN3" value="${entidadN3Aux}" />
                                    <h3 class="entidad">${entidadN3}</h3>
                                </c:if>
                                <c:if test="${entidadN3 eq entidadN3Aux}">
                                    <c:if test="${not (entidadN4 eq entidadN4Aux)}">
                                        <c:set var="entidadN4" value="${entidadN4Aux}" />
                                        <h4 class="detalle-entidad">${entidadN4}</h4>
                                    </c:if>
                                    <c:if test="${entidadN4 eq entidadN4Aux}">
                                    </c:if>
                                </c:if>
                            </c:if>
                            <div class="anuncio-contenido">${resourceAnuncio.value.TextoDelAnuncio}</div>
                            <p class="separadorbops2">
                                    <img class="separador" title="separador-bop" src="<cms:link>/sites/gestion-bop-dipu-sevilla/.content/.template-elements/assets/separador-bop.jpg</cms:link>"
                                 alt="separador-bop" />
                                 </p>
                        </c:forEach>
                    </div>
                    <div class="tasas-bop-wrapper">
                        <div class="tasas-bop">
                            <div class="titular">
                                <h2>
                                    TASAS CORRESPONDIENTES AL<br />«BOLETÍN OFICIAL» DE LA PROVINCIA DE SEVILLA
                                </h2>
                            </div>
                            <div class="precios">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <p>Inserción anuncio, línea ordinaria......2,10</p>
                                                
                                            </td>
                                            <td>
                                                <p>Importe mínimo de inserción.................18,41</p>
                                                
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <p>Inserción anuncio, línea urgente........3,25</p>
                                                
                                            </td>
                                            <td>
                                                <p>Venta de CD’s publicaciones anuales.....5,72</p>
                                                
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="nota-final">
                                <p class="primer-parrafo">Las solicitudes de inserción de anuncios, así como la correspondencia de tipo administrativo
                                    y económico, se dirigirán al «Boletín Oficial» de la provincia de Sevilla, avenida Menéndez y Pelayo, 32.
                                    41071-Sevilla.</p>
                                    <p class="separadorbops">
                                    <img class="separador" title="separador-bop" src="<cms:link>/sites/gestion-bop-dipu-sevilla/.content/.template-elements/assets/separador-bop.jpg</cms:link>"
                                 alt="separador-bop" />
                                 </p>
                                <p>Dirección del «Boletín Oficial» de la provincia de Sevilla: Ctra. Isla Menor, s/n. (Bellavista), 41014-Sevilla.
                                    Teléfonos: 954 554 133 - 34 - 35 - 39. Faxes: 954 693 857 - 954 *0 649. Correo electrónico: bop@dipusevilla.es</p>
                            </div>
                        </div>
                        <div class="imprenta">
                            <p>Diputación Provincial - Imprenta</p>
                        </div>
                    </div>
                </div>
            </div>
            </body>
        </cms:formatter>
        </html>
    </cms:bundle>