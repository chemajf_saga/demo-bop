<%@ tag trimDirectiveWhitespaces="true" pageEncoding="UTF-8" description="Muestra el detalle de la entidad a partir del arbol de la entidad"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ attribute name="entidades" type="java.lang.String" required="true" rtexprvalue="true"
    description="Rutas de las categorías de las entidades separadas por ','"%>
<%@ attribute name="nivel" type="java.lang.String" required="true" rtexprvalue="true"
    description="Nivel a partir del cual se mostrará el detalle de la entidad"%>
<%@ attribute name="concreto" type="java.lang.String" required="false" rtexprvalue="true" description="Si se indica, devuelve SOLO el nivel indicado"%>
<%
    String entidadFinal = "";
    String entidadesStr = entidades;
    entidadesStr = entidadesStr.startsWith("/") ? entidadesStr.substring(1) : entidadesStr;
    String[] entidades = entidadesStr.split("/");
    String[] entidadesFinales = new String[entidades.length];
    int j = entidades.length;
    for (int i = 0; i < entidades.length; i++) {
        String aux = "";
        for (int t = 0; t < j; t++) {
            aux += "/" + entidades[t];
        }
        if (aux.equalsIgnoreCase("/")) {

        }
        entidadesFinales[j - 1] = aux;
        j--;
    }

    int nivelInt = Integer.parseInt(nivel);
    if (concreto != null && !concreto.equalsIgnoreCase("")) {
        try {
            jspContext.setAttribute("aux2", entidadesFinales[nivelInt]);
%>
            <cms:property name="Title" file="${aux2}" />
<%
    } catch (Exception e) {
        }

    } else {
        for (int k = nivelInt; k < entidadesFinales.length; k++) {
            jspContext.setAttribute("aux2", entidadesFinales[k]);
%>
<c:set var="entidadTitAux">
    <cms:property name="Title" file="${aux2}" />
</c:set>
<%
    entidadFinal += "/" + jspContext.getAttribute("entidadTitAux").toString();
        }
        out.println(entidadFinal);
    }
%>
