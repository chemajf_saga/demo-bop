<%@tag import="java.util.Date"%>
<%@ tag trimDirectiveWhitespaces="true" pageEncoding="UTF-8" description="Muestra el detalle de la entidad a partir del arbol de la entidad"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!--  -->
<%@ attribute name="fecha" type="java.lang.String" required="true" rtexprvalue="true" description="Fecha a formatear"%>
<%@ attribute name="patron" type="java.lang.String" required="true" rtexprvalue="true" description="Patron del formato"%>
<%@ attribute name="may" type="java.lang.String" required="false" rtexprvalue="true" description="Si se indica, la primera letra será mayúscula"%>
<%
    try {
        Date dateJ = new Date(Long.parseLong(fecha));
        jspContext.setAttribute("dateJ", dateJ);
    } catch (Exception e) {
        e.printStackTrace();
    }
%>
<c:set var="fechaPublicacionFormated">
    <fmt:formatDate pattern="${patron}" value="${dateJ}" />
</c:set>
<%
    if (may != null && !may.equals("")) {
        String f = jspContext.getAttribute("fechaPublicacionFormated").toString();
        out.print(f.substring(0, 1).toUpperCase() + f.substring(1, f.length()));
    } else {
        out.print(jspContext.getAttribute("fechaPublicacionFormated").toString());
    }
%>