<%@ tag trimDirectiveWhitespaces="true" pageEncoding="UTF-8" description="Cambiamos el estado de un anuncio"%>
<%@tag import="org.apache.commons.lang3.StringUtils"%>
<%----%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%----%>
<%@ attribute name="estado" type="java.lang.String" required="true" rtexprvalue="true"
    description="Estado actual: nuevo, pendiente_pago, en_maquetacion, rechazado o acetado"%>
<%----%>
<%
    if (estado != null && StringUtils.isNotEmpty(estado)) {
        if (estado.equalsIgnoreCase("nuevo")) {
            out.print("Nuevo");
        } else {
            if (estado.equalsIgnoreCase("pendiente_pago")) {
                out.print("Pendiente de Pago");
            } else {
                if (estado.equalsIgnoreCase("en_maquetacion")) {
                    out.print("En maquetación");
                } else {
                    if (estado.equalsIgnoreCase("rechazado")) {
                        out.print("Rechazado");
                    } else {
                        if (estado.equalsIgnoreCase("acetado")) {
                            out.print("Aceptado");
                        } else {
                            if (estado.equalsIgnoreCase("aceptado")) {
                                out.print("Aceptado");
                            } else {
                                if (estado.equalsIgnoreCase("preparacion")) {
                                    out.print("En Preparación");
                                } else {
                                    if (estado.equalsIgnoreCase("pendiente_firma")) {
                                        out.print("Pendiente de Firma");
                                    } else {
                                        if (estado.equalsIgnoreCase("firmado_enviado")) {
                                            out.print("Firmado y Enviado");
                                        } else {
                                            if (estado.equalsIgnoreCase("devuelto")) {
                                                out.print("Devuelto");
                                            } else {
                                                if (estado.equalsIgnoreCase("en_curso")) {
                                                    out.print("En Curso");
                                                } else {
                                                    if (estado.equalsIgnoreCase("p_publicacion")) {
                                                        out.print("Pendiente de Publicación");
                                                    } else {
                                                        if (estado.equalsIgnoreCase("publicado")) {
                                                            out.print("Publicado");
                                                        } else {
                                                            out.print("");
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    } else {
        out.print("");

    }
%>