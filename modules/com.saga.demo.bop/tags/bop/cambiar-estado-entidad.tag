<%@tag import="org.apache.commons.lang3.StringUtils"%>
<%@ tag trimDirectiveWhitespaces="true" pageEncoding="UTF-8" description="Cambiamos el estado de un anuncio"%>
<%@tag import="org.opencms.main.OpenCms"%>
<%@tag import="com.saga.demo.bop.manager.resources.ResourceContent"%>
<%@tag import="org.opencms.workplace.CmsWorkplaceAction"%>
<%@tag import="org.opencms.file.CmsResource"%>
<%@tag import="org.opencms.file.CmsObject"%>
<%----%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%----%>
<%@ attribute name="fileName" type="java.lang.String" required="true" rtexprvalue="true" description="FileName del recurso a tratar"%>
<%@ attribute name="cambio" type="java.lang.String" required="false" rtexprvalue="true"
    description="Indica el cambio de estado a realizar. Este puede ser: preparacion, pendiente_firma, firmado_enviado, aceptado o devuelto"%>
<%----%>
<%
    try {
        CmsObject cmsObjectAdmin = CmsWorkplaceAction.getInstance().getCmsAdminObject();

        // Cambiamos a offLine
        if (cmsObjectAdmin.getRequestContext().getCurrentProject().getName().equals("Online")) {
            cmsObjectAdmin.getRequestContext().setCurrentProject(cmsObjectAdmin.readProject("Offline"));
        }

        // Abrir recurso
        CmsResource anuncio = cmsObjectAdmin.readResource("/sites/gestion-bop-dipu-sevilla" + fileName);
        out.print(anuncio.getName());
        ResourceContent resourceContent = new ResourceContent(cmsObjectAdmin, anuncio, "es");

        // REALIZAMOS LOS CAMBIOS
        if (cambio != null && StringUtils.isNotEmpty(cambio)) {
            
            try {
                resourceContent.addValue("EstadoParaElAnunciante", cambio);
            } catch (Exception e) {
                resourceContent.setStringValue("EstadoParaElAnunciante", cambio);
            }

            // Guardamos los cambios efectuados
            resourceContent.saveXml();

            // Si todo ha ido bien, publicamos
            OpenCms.getPublishManager().publishResource(cmsObjectAdmin, anuncio.getRootPath());
            // Esperamos la publicacion
            OpenCms.getPublishManager().waitWhileRunning();
            out.print(cambio);
        }
    } catch (Exception e) {
        e.printStackTrace();
    }
%>