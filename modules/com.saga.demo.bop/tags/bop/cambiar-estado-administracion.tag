<%@tag import="java.util.Date"%>
<%@tag import="org.apache.commons.lang3.StringUtils"%>
<%@ tag trimDirectiveWhitespaces="true" pageEncoding="UTF-8" description="Cambiamos el estado de un anuncio"%>
<%@tag import="org.opencms.main.OpenCms"%>
<%@tag import="com.saga.demo.bop.manager.resources.ResourceContent"%>
<%@tag import="org.opencms.workplace.CmsWorkplaceAction"%>
<%@tag import="org.opencms.file.CmsResource"%>
<%@tag import="org.opencms.file.CmsObject"%>
<%----%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%----%>
<%@ attribute name="fileName" type="java.lang.String" required="true" rtexprvalue="true" description="FileName del recurso a tratar"%>
<%@ attribute name="docRevisada" type="java.lang.String" required="false" rtexprvalue="true" description="Indica si se ha revisado la documentacion"%>
<%@ attribute name="anuncioRevisado" type="java.lang.String" required="false" rtexprvalue="true" description="Indica si se ha revisado el anuncio"%>
<%@ attribute name="precioCorrecto" type="java.lang.String" required="false" rtexprvalue="true" description="Indica si el precio es correcto"%>
<%@ attribute name="anuncioPagado" type="java.lang.String" required="false" rtexprvalue="true" description="Indica si el anuncio se ha pagado"%>
<%@ attribute name="motivoRechazo" type="java.lang.String" required="false" rtexprvalue="true" description="Indica si el motivo del rechazo"%>
<%@ attribute name="cambio" type="java.lang.String" required="false" rtexprvalue="true"
    description="Indica el cambio de estado a realizar. Este puede ser: nuevo, pendiente_pago, en_maquetacion, rechazado o acetado"%>
<%----%>
<%
    try {
        CmsObject cmsObjectAdmin = CmsWorkplaceAction.getInstance().getCmsAdminObject();

        // Cambiamos a offLine
        if (cmsObjectAdmin.getRequestContext().getCurrentProject().getName().equals("Online")) {
            cmsObjectAdmin.getRequestContext().setCurrentProject(cmsObjectAdmin.readProject("Offline"));
        }

        // Abrir recurso
        CmsResource anuncio = cmsObjectAdmin.readResource("/sites/gestion-bop-dipu-sevilla" + fileName);
        out.print(anuncio.getName());
        ResourceContent resourceContent = new ResourceContent(cmsObjectAdmin, anuncio, "es");

        // REALIZAMOS LOS CAMBIOS
        if (cambio != null && StringUtils.isNotEmpty(cambio)) {
            resourceContent.setStringValue("EstadoParaElTramitador", cambio);

            // Si el cambio en en_maquetacion o pendiente_pago
            if (cambio.equalsIgnoreCase("en_maquetacion") || cambio.equalsIgnoreCase("pendiente_pago")) {
                if (StringUtils.isNotEmpty(docRevisada) && docRevisada.equalsIgnoreCase("on")) {
                    resourceContent.setStringValue("DocumentacionRevisada", "true");
                }
                if (StringUtils.isNotEmpty(anuncioRevisado) && anuncioRevisado.equalsIgnoreCase("on")) {
                    resourceContent.setStringValue("AnuncioRevisado", "true");
                }
                /* if (StringUtils.isNotEmpty(precioCorrecto) && precioCorrecto.equalsIgnoreCase("on")) {
                    resourceContent.setStringValue("", "true");
                
                } */
                if (StringUtils.isNotEmpty(anuncioPagado) && anuncioPagado.equalsIgnoreCase("on")) {
                    resourceContent.setStringValue("AnuncioPagado", "true");
                }
            }

            // Si es un rechazo, indicamos el motivo
            if (cambio.equalsIgnoreCase("rechazado")) {
                try {
                    resourceContent.addValue("MotivoRechazo", motivoRechazo);
                } catch (Exception e) {
                    resourceContent.setStringValue("MotivoRechazo", motivoRechazo);
                }
                // Indicamos la fecha del rechazo
                Date hoy = new Date();
                try {
                    resourceContent.addValue("FechaRechazo", hoy.getTime() + "");
                } catch (Exception e) {
                    resourceContent.setStringValue("FechaRechazo", hoy.getTime() + "");
                }
                // Cambiamos el Estado para el Anunciante
                resourceContent.setStringValue("EstadoParaElAnunciante", "devuelto");

            }
            // Si es Aceptado, cambiamos el Estado para el Anunciante
            if (cambio.equalsIgnoreCase("acetado")) {
                resourceContent.setStringValue("EstadoParaElAnunciante", "aceptado");
            }

            // Guardamos los cambios efectuados
            resourceContent.saveXml();

            // Si todo ha ido bien, publicamos
            OpenCms.getPublishManager().publishResource(cmsObjectAdmin, anuncio.getRootPath());
            // Esperamos la publicacion
            OpenCms.getPublishManager().waitWhileRunning();
            out.print(cambio);
        }
    } catch (Exception e) {
        e.printStackTrace();
        out.print("0");
    }
%>