<%@page import="java.util.Date"%>
<%@page import="java.lang.Long"%>
<%@page buffer="none" session="false" trimDirectiveWhitespaces="true"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="sg" tagdir="/WEB-INF/tags/core/templates"%>
<%@taglib prefix="bop" tagdir="/WEB-INF/tags/bop"%>
<%-- Formateamos fecha --%>
<c:set var="dateFormated">
    <bop:fecha fecha="${date}" patron="dd/MM/yyyy" />
</c:set>
<%-- Tratamos el detalle de entidad (categoria) --%>
<c:set var="entidadFinal">
    <bop:entidad nivel="2" entidades="${resource.value.DetalleEntidad}" />
</c:set>
<%-- Plazo de publicacion --%>
<c:set var="plazoPublicacion" value="" />
<c:if test="${resource.value.PlazoDePublicacion.value.PlazoOrdinario.isSet}">
    <c:set var="plazoPublicacion" value="Ordinario" />
</c:if>
<c:if test="${resource.value.PlazoDePublicacion.value.PlazoUrgente.isSet}">
    <c:set var="plazoPublicacion" value="Urgente" />
</c:if>
<c:if test="${resource.value.PlazoDePublicacion.value.PlazoEspecifico.isSet}">
    <c:set var="plazoPublicacion">
        <bop:fecha fecha="${resource.value.PlazoDePublicacion.value.PlazoEspecifico.value.PlazoEspecificoValue}" patron="dd/MM/yyyy" />
    </c:set>
</c:if>
<%-- ${resource.value.Descripcion} --%>
<div class="anuncio">
    <div class="fecha-entidad">
        <div class="info-entidad">
            <p class="fecha-creacion">
                <span class="fa fa-calendar" aria-hidden="true">&nbsp;</span>${dateFormated}&nbsp;
            </p>
            <a href="<cms:link>${linkDetail}</cms:link>" title="${title}"><h5 class="title-anuncio">${title}</h5></a>
        </div>
    </div>
    <div class="resumen-anuncio">
        <p>${description}</p>
    </div>
    <div class="info-add">
        <div class="tramitador">
            <p>
                <span class="fa fa-user" aria-hidden="true">&nbsp;</span>${resource.value.Tramitador.value.Nombre}
            </p>
        </div>
        <div class="tramitador">
            <p>
                <span class="fa fa-folder" aria-hidden="true">&nbsp;</span>${entidadFinal}
            </p>
        </div>
        <div class="plazo-publicacion">
            <p>
                <span class="fa fa-calendar" aria-hidden="true">&nbsp;</span>Plazo publicación:
                <c:if test="${not empty plazoPublicacion}">${plazoPublicacion}</c:if>
                <c:if test="${empty plazoPublicacion}">no indicado</c:if>
            </p>
        </div>
    </div>
</div>