<%@page import="java.util.Date"%>
<%@page import="java.lang.Long"%>
<%@page buffer="none" session="false" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="cms" uri="http://www.opencms.org/taglib/cms"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sg" tagdir="/WEB-INF/tags/core/templates"%>
<%@taglib prefix="bop" tagdir="/WEB-INF/tags/bop"%>
<%-- Formateamos fecha --%>
<c:set var="dateFormated">
    <bop:fecha fecha="${resource.value.FechaPublicacion}" patron="dd/MM/yyyy" />
</c:set>
<%-- ${resource.value.Descripcion} --%>
<c:set var="title" value="BOP: ${resource.value.Anyo} - ${resource.value.CodigoBop}" />
<div class="anuncio bop-list">
    <div class="fecha-entidad">
        <div class="info-entidad">
            <a href="<cms:link>${linkDetail}</cms:link>" title="${title}"><h5 class="title-anuncio">${title}</h5></a> &nbsp;&nbsp;
            <p class="fecha-creacion" style="min-width: 99.7px;">
                <c:if test="${resource.value.FechaPublicacion.isSet}">
                    <span class="fa fa-calendar" aria-hidden="true">&nbsp;</span>${dateFormated}
                </c:if>
                <c:if test="${not resource.value.FechaPublicacion.isSet}">
                    <span class="fa fa-calendar" aria-hidden="true">&nbsp;</span>Sin fecha
                </c:if>
                &nbsp;&nbsp;
            </p>
        </div>
        <div class="info-add">
            <div class="tramitador">
                <p>
                    <span class="fa fa-folder" aria-hidden="true">&nbsp;</span>${fn:length(resource.valueList.Anuncios)} anuncio<c:if test="${fn:length(resource.valueList.Anuncios) > 1}">s</c:if>
                </p>
            </div>
        </div>
    </div>
</div>