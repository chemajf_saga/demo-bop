/**
 * Package com.saga.demo.bop
 * File Constantes.java
 * Created at 25 feb. 2019
 * Created by chema.jimenez
 */
package com.saga.demo.bop;

/**
 * @author chema.jimenez
 * @since 25 feb. 2019
 * 
 *        <pre>
 * 
 *        </pre>
 */
public class Constantes {

    static final String NUEVO = "nuevo";
    static final String PENDIENTE_PAGO = "pendiente_pago";
    static final String EN_MAQUETACION = "en_maquetacion";
    static final String RECHAZADO = "rechazado";
    static final String ACETADO = "acetado";

}
