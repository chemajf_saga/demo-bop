/**
 * Package com.saga.demo.bop.exception
 * File BadRequestException.java
 * Created at 7 may. 2018
 * Created by chema.jimenez
 */
package com.saga.demo.bop.exception;

/**
 * @author chema.jimenez
 * @since 7 may. 2018
 *
 *        <pre>
 *
 *        </pre>
 */
public class BadRequestException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = -6364291283530479612L;

    /**
     * @param msg
     */
    public BadRequestException(String msg) {
        super(msg);
    }
}
